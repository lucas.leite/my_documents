import Vapor
import FluentPostgreSQL

final class User: PostgreSQLModel {
    var id: Int?
    var name: String
    var email: String
    var phone: String
    
    var createdAt: Date?
    var updatedAt: Date?
    
    init(name: String, email: String, phone: String) {
        self.name = name
        self.email = email
        self.phone = phone
    }
}

extension User {
    var documents: Children<User, Document> {
        return children(\.userID)
    }
    
    static var createdAtKey: TimestampKey? = \.createdAt
    
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

extension User {
    final class Update: Content {
        var name: String?
        var email: String?
        var phone: String?
    }
    
    final class Detail: Content {
        var id: Int?
        var name: String
        var email: String
        var phone: String
        var documents: [Document]
        
        init(user: User, documents: [Document]) {
            self.id = user.id
            self.name = user.name
            self.email = user.email
            self.phone = user.phone
            self.documents = documents
        }
    }
}

extension User: Content {}

extension User: Migration {}

extension User: Parameter {}
