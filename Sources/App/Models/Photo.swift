import Vapor
import FluentPostgreSQL

final class Photo: PostgreSQLModel {
    var id: Int?
    var image: Data
    
    var documentID: Document.ID
    
    var createdAt: Date?
    var updatedAt: Date?
    
    init(image: Data, documentID: Document.ID) {
        self.image = image
        self.documentID = documentID
    }
}

extension Photo {
    var document: Parent<Photo, Document> {
        return parent(\.documentID)
    }
    
    static var createdAtKey: TimestampKey? = \.createdAt
    
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

extension Photo {
    final class Update: Content {
        var image: Data?
        var documentID: Document.ID?
    }
}

extension Photo: Migration {
    static func prepare(on conn: PostgreSQLConnection) -> Future<Void> {
        return Database.create(self, on: conn) { builder in
            try addProperties(to: builder)
            builder.unique(on: \.documentID)
            builder.reference(from: \.documentID, to: \Document.id, onDelete: .cascade)
        }
    }
}

extension Photo: Content {}

extension Photo: Parameter {}
