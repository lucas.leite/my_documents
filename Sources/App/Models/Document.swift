import Vapor
import FluentPostgreSQL

final class Document: PostgreSQLModel {
    var id: Int?
    var name: String
    var data: String
    
    var userID: User.ID
    
    var createdAt: Date?
    var updatedAt: Date?
    
    init(name: String, data: String, userID: User.ID) {
        self.name = name
        self.data = data
        self.userID = userID
    }
}

extension Document {
    var user: Parent<Document, User> {
        return parent(\.userID)
    }
    
    var photo: Children<Document, Photo> {
        return children(\.documentID)
    }
    
    static var createdAtKey: TimestampKey? = \.createdAt
    
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

extension Document {
    final class Update: Content {
        var name: String?
        var data: String?
        var userID: User.ID?
    }
}

extension Document: Migration {
    static func prepare(on conn: PostgreSQLConnection) -> Future<Void> {
        return Database.create(self, on: conn) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.userID, to: \User.id)
        }
    }
}

extension Document: Content {}

extension Document: Parameter {}
