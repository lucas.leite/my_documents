import FluentPostgreSQL
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first
    try services.register(FluentPostgreSQLProvider())
    
    var serverConfig = NIOServerConfig.default()
    serverConfig.maxBodySize = 10_000_000
    services.register(serverConfig)

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
//    / middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    services.register(setupDatabaseConfig(env))
    services.register(setupMigrationsConfig())
}

//func setupContentConfig() -> ContentConfig {
//    var contentConfig = ContentConfig.default()
//
//    let jsonEncoder = JSONEncoder()
//    jsonEncoder.keyEncodingStrategy = .convertToSnakeCase
//    let jsonDecoder = JSONDecoder()
//    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
//
//    contentConfig.use(encoder: jsonEncoder, for: .json)
//    contentConfig.use(decoder: jsonDecoder, for: .json)
//
//    return contentConfig
//}

/// Configure database
func setupDatabaseConfig(_ env: Environment = .production) -> DatabasesConfig {
    var databases = DatabasesConfig()
    
    let databaseConfig: PostgreSQLDatabaseConfig
    if let url = Environment.get("DATABASE_URL") {
        databaseConfig = PostgreSQLDatabaseConfig(url: url)!
    } else {
        databaseConfig = PostgreSQLDatabaseConfig(hostname: "localhost", port: 5433, username: "vapor", database: "vapor", password: "password")
    }
    
    let database = PostgreSQLDatabase(config: databaseConfig)
    databases.add(database: database, as: .psql)
    
    return databases
}

/// Configure migrations
func setupMigrationsConfig() -> MigrationConfig {
    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .psql)
    migrations.add(model: Document.self, database: .psql)
    migrations.add(model: Photo.self, database: .psql)
    
    return migrations
}
