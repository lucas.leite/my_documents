import Vapor

struct DocumentsController: RouteCollection {
    func boot(router: Router) throws {
        let documentsRouter = router.grouped("documents")
        documentsRouter.get(Document.parameter, use: get)
        documentsRouter.get(use: getAll)
        documentsRouter.post(use: create)
        documentsRouter.put(Document.parameter, use: update)
        documentsRouter.patch(Document.parameter, use: update)
        documentsRouter.delete(Document.parameter, use: delete)
        documentsRouter.get(Document.parameter, "photo", use: getPhoto)
    }
    
    func get(_ req: Request) throws -> Future<Document> {
        return try req.parameters.next(Document.self)
    }
    
    func getAll(_ req: Request) -> Future<[Document]> {
        return Document.query(on: req).all()
    }
    
    func create(_ req: Request) throws -> Future<Document> {
        return try req.content.decode(Document.self).save(on: req)
    }
    
    func update(_ req: Request) throws -> Future<Document> {
        switch req.http.method {
        case .PUT:
            return try flatMap(req.parameters.next(Document.self), req.content.decode(Document.self)) { document, newDocument in
                document.name = newDocument.name
                document.data = newDocument.data
                document.userID = newDocument.userID
                
                return document.update(on: req)
            }
        case .PATCH:
            return try flatMap(req.parameters.next(Document.self), req.content.decode(Document.Update.self)) { document, newDocument in
                document.name = newDocument.name ?? document.name
                document.data = newDocument.data ?? document.data
                document.userID = newDocument.userID ?? document.userID
                
                return document.update(on: req)
            }
        default:
            throw Abort(.badRequest)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Document.self).delete(on: req).transform(to: .noContent)
    }
    
    func getPhoto(_ req: Request) throws -> Future<Photo> {
        return try req.parameters.next(Document.self).flatMap { document in
            return try document.photo.query(on: req).first()
        }.map { photo in
            guard let photo = photo else {
                throw Abort(.notFound)
            }
            return photo
        }
    }
}
