import Vapor

struct PhotosController: RouteCollection {
    func boot(router: Router) throws {
        let photosRouter = router.grouped("photos")
        photosRouter.get(Photo.parameter, use: get)
        photosRouter.get(use: getAll)
        photosRouter.post(use: create)
        photosRouter.put(Photo.parameter, use: update)
        photosRouter.patch(Photo.parameter, use: update)
        photosRouter.delete(Photo.parameter, use: delete)
    }
    
    func get(_ req: Request) throws -> Future<Photo> {
        return try req.parameters.next(Photo.self)
    }
    
    func getAll(_ req: Request) -> Future<[Photo]> {
        return Photo.query(on: req).all()
    }
    
    func create(_ req: Request) throws -> Future<Photo> {
        return try req.content.decode(Photo.self).save(on: req)
    }
    
    func update(_ req: Request) throws -> Future<Photo> {
        switch req.http.method {
        case .PUT:
            return try flatMap(req.parameters.next(Photo.self), req.content.decode(Photo.self)) { photo, newPhoto in
                photo.image = newPhoto.image
                photo.documentID = newPhoto.documentID
                
                return photo.update(on: req)
            }
        case .PATCH:
            return try flatMap(req.parameters.next(Photo.self), req.content.decode(Photo.Update.self)) { photo, newPhoto in
                photo.image = newPhoto.image ?? photo.image
                photo.documentID = newPhoto.documentID ?? photo.documentID
                
                return photo.update(on: req)
            }
        default:
            throw Abort(.badRequest)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Photo.self).delete(on: req).transform(to: .noContent)
    }
}
