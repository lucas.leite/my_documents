import Vapor

struct UsersController: RouteCollection {
    func boot(router: Router) throws {
        let usersRouter = router.grouped("users")
        usersRouter.get(use: getAll)
        usersRouter.get(User.parameter, use: get)
        usersRouter.post(use: create)
        usersRouter.put(User.parameter, use: update)
        usersRouter.patch(User.parameter, use: update)
        usersRouter.delete(User.parameter, use: delete)
        usersRouter.get(User.parameter, "documents", use: getDocuments)
    }
    
    func getAll(_ req: Request) -> Future<[User]> {
        return User.query(on: req).all()
    }
    
    func get(_ req: Request) throws -> Future<User.Detail> {
        return try req.parameters.next(User.self).flatMap { user in
            return try user.documents.query(on: req).all().map { documents in
                return User.Detail(user: user, documents: documents)
            }
        }
    }
    
    func create(_ req: Request) throws -> Future<User> {
        return try req.content.decode(User.self).save(on: req)
    }
    
    func update(_ req: Request) throws -> Future<User> {
        switch req.http.method {
        case .PUT:
            return try flatMap(req.parameters.next(User.self), req.content.decode(User.self)) { user, newUser in
                user.name = newUser.name
                user.email = newUser.email
                user.phone = newUser.phone
                
                return user.update(on: req)
            }
        case .PATCH:
            return try flatMap(req.parameters.next(User.self), req.content.decode(User.Update.self)) { user, newUser in
                user.name = newUser.name ?? user.name
                user.email = newUser.email ?? user.email
                user.phone = newUser.phone ?? user.phone
                
                return user.update(on: req)
            }
        default:
            throw Abort(.badRequest)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(User.self).delete(on: req).transform(to: .noContent)
    }
    
    func getDocuments(_ req: Request) throws -> Future<[Document]> {
        return try req.parameters.next(User.self).flatMap { user in
            return try user.documents.query(on: req).all()
        }
    }
}
