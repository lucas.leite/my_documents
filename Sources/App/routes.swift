import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let usersController = UsersController()
    try router.register(collection: usersController)
    
    let documentsController = DocumentsController()
    try router.register(collection: documentsController)
    
    let photosController = PhotosController()
    try router.register(collection: photosController)
}
